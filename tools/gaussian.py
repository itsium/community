import Image
import ImageFilter


class MyGaussianBlur(ImageFilter.Filter):
    name = 'GaussianBlur'

    def __init__(self, radius=2):
        self.radius = radius

    def filter(self, image):
        return image.gaussian_blur(self.radius)


img = Image.open('test.jpg')
#size = 968, 648  # 3872, 2592
#img.thumbnail(size, Image.ANTIALIAS)
blurred = img.filter(MyGaussianBlur(radius=10))
#blurred.save('test-gaussian.jpg', 'JPEG', quality=100)
blurred.save('test-gaussian.png', 'PNG', optimize=True)
