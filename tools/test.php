<?php
/*
$str = 'version(),null';

for ($i = 0; $i < 100; ++$i) {
  echo $str, chr(10);
  $str .= ',null';
}
exit;
*/
for ($j = 0; $j < 13; ++$j) {
  $tab = array();
  for ($i = 0; $i < 13; ++$i) {
    if ($i === $j) {
      $tab[] = 'version()';
    } else {
      $tab[] = 'null';
    }
  }
  echo implode($tab, ','), chr(10);
}