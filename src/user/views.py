"""
User views.
"""
import api
import manager

from forms import SignupForm, SigninForm, ProfileForm
from helpers import login_required
from flask.ext.login import login_user, current_user, logout_user
from flask import request, Blueprint, render_template, redirect, url_for


user = Blueprint('user', __name__,
                 static_folder='static',
                 static_url_path='/static',
                 template_folder='templates',
                 url_prefix='/user')


@user.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    """Show user profile."""
    if request.method == 'POST':
        form = ProfileForm(request.form, current_user)
        print repr(form.avatar.errors)
        print repr(form.firstname.errors)
        print repr(request.files)
        if form.validate():
            api.update_current_user(form)
    else:
        form = ProfileForm(None, current_user)
    return render_template('profile.html', form=form)


@user.route('/test')
def testass():
    print repr(current_user)
    print dir(current_user)
    return 'Test'


@user.route('/signout')
@login_required
def signout():
    logout_user()
    return redirect(url_for('index'))


@user.route('/signin', methods=['GET', 'POST'])
def signin():
    """Sign in user."""
    if request.method == 'POST':
        form = SigninForm(request.form)
        if form.validate():
            user = api.get(form)
            if user is not None:
                login_user(user)
                return redirect(request.args.get('next') or
                                url_for('user.profile'))
    else:
        form = SigninForm()
    return render_template('signin.html', form=form)


@user.route('/signup', methods=['GET', 'POST'])
def signup():
    """Sign up user."""
    if request.method == 'POST':
        form = SignupForm(request.form)
        if form.validate():
            result = api.add(form)
            if result[0] is not False:
                #db_session.add(user)
                return redirect(url_for('user.signin'))
            else:
                form.email.errors = [result[1]]
    else:
        form = SignupForm()
    return render_template('signup.html', form=form)
