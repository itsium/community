"""
User model.
"""
import datetime

from flask import url_for
from app import db


class User(db.Document):
    id_user = db.SequenceField()
    created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
    email = db.EmailField(required=True)
    password = db.StringField(max_length=255, required=True)
    lastname = db.StringField(max_length=255, required=True)
    firstname = db.StringField(max_length=255, required=True)
    birthday = db.DateTimeField(required=False)

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def is_authenticated(self):
        return True

    def get_id(self):
        return self.id_user

    def get_absolute_url(self):
        return url_for('user', kwargs={'firstname': self.firstname})

    def __unicode__(self):
        return '%s %s' % (self.lastname, self.firstname)

    meta = {
        'allow_inheritance': True,
        'indexes': ['-created_at'],
        'ordering': ['-created_at']
    }
