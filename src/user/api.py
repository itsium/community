"""
User api.
"""
import hashlib

from models import User
from app import app
from flask.ext.login import current_user
from flask.ext.babel import gettext


def hash_password(password):
    """Hash password and return it."""
    m = hashlib.sha1()
    m.update('%s%s' % (app.config['SECRET_KEY'], password))
    return m.hexdigest()


def add(form):
    """Add user from given form data."""
    exists = len(User.objects(email=form.email.data))
    if exists > 0:
        return (False, gettext('Email already in use.'))
    user = User(email=form.email.data,
                firstname=form.firstname.data,
                lastname=form.lastname.data,
                password=hash_password(form.password.data))
    result = user.save()
    print repr(result)
    return (result, '')


def update_current_user(form):
    """Update user from given form data."""
    data = form.data
    if len(data.get('password', '')) > 0:
        data['password'] = hash_password(form.password.data)
    else:
        del data['password']
    for k, v in data.iteritems():
        setattr(current_user, k, v)
    current_user.save()
    return True


def get(form):
    """Get user from given form data."""
    password = hash_password(form.password.data)
    exists = User.objects(email=form.email.data,
                          password=password)
    if len(exists) > 0:
        return exists.first()
    return
