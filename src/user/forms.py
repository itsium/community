"""
User forms.
"""
import datetime

from flask.ext.uploads import UploadSet, IMAGES
from flask.ext.wtf import (Form, TextField, PasswordField,
                           BooleanField, DateField,
                           FileField, validators,
                           file_allowed, file_required)
from flask.ext.wtf.html5 import EmailField
from flask.ext.babel import gettext
from helpers import DateRange


class SignupForm(Form):
    """Sign up form."""
    email = EmailField(gettext('Email Address'), [
        validators.Required(),
        validators.Length(min=6, max=55)
    ])
    firstname = TextField(gettext('Firstname'), [
        validators.Required(),
        validators.Length(min=2, max=35)
    ])
    lastname = TextField(gettext('Lastname'), [
        validators.Required(),
        validators.Length(min=2, max=35)
    ])
    password = PasswordField(gettext('Password'), [
        validators.Required(),
        validators.EqualTo('confirm', message=gettext('Passwords must match'))
    ])
    confirm = PasswordField(gettext('Repeat Password'))
    accept_tos = BooleanField(gettext('I accept the TOS'), [
        validators.Required()
    ])


class SigninForm(Form):
    """Sign in form."""
    email = EmailField(gettext('Email Address'), [
        validators.Required(),
        validators.Length(min=6, max=55)
    ])
    password = PasswordField(gettext('Password'), [
        validators.Required()
    ])


images = UploadSet('photos', IMAGES)


class ProfileForm(Form):
    """Profile form."""
    email = EmailField(gettext('Email Address'), [
        validators.Required(),
        validators.Length(min=6, max=55)
    ])
    firstname = TextField(gettext('Firstname'), [
        validators.Required(),
        validators.Length(min=2, max=35)
    ])
    lastname = TextField(gettext('Lastname'), [
        validators.Required(),
        validators.Length(min=2, max=35)
    ])
    birthday = DateField(gettext('Birthday'), format='%d/%m/%Y',
                         validators=[
                         DateRange(
                         min=datetime.date(1900, 01, 01),
                         max=datetime.date.today())
                         ])
    avatar = FileField(gettext('Upload your avatar'),
                       validators=[file_required(),
                                   file_allowed(images, gettext('Images only!'))])
    password = PasswordField(gettext('Password'))
