"""
User login manager.
"""
from app import app
from flask import redirect, url_for
from flask.ext.login import LoginManager
from models import User


login_manager = LoginManager()
login_manager.setup_app(app)


@login_manager.user_loader
def load_user(id_user):
    exists = User.objects(id_user=id_user)
    if len(exists) > 0:
        return exists.first()
    redirect(url_for('user.signin'))
    return
