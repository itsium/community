"""
Application bootstrap.
"""
import os

#from __future__ import unicode_literals
from flask import Flask, redirect, url_for
from flask.ext.mongoengine import MongoEngine
from flask_debugtoolbar import DebugToolbarExtension


app = Flask(__name__)
app.config['MONGODB_DB'] = 'around_dev'
app.config['SECRET_KEY'] = 'cIGihy4np6N1Sve3WqNGc63L5TIaHfvB'
app.config['CSRF_ENABLED'] = False
app.config['DEBUG'] = True
app.config['DEBUG_TB_PROFILER_ENABLED'] = True
app.config['UPLOAD_FOLDER'] = os.path.join(os.path.realpath('.'), 'static/medias/')
app.jinja_env.add_extension('vendors.jinja2htmlcompress.HTMLCompress')
app.jinja_env.add_extension('jinja2.ext.do')


import translation


db = MongoEngine(app)

from user.views import user
app.register_blueprint(user)

#toolbar = DebugToolbarExtension(app)


@app.route('/')
def index():
    return redirect(url_for('user.signin'))

if __name__ == '__main__':
    app.run()
