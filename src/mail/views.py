from flask_mail import Mail, Message


mail = Mail(app)


@app.route("/")
def hello():
    return 'Hello World!'


@app.route('/mail/test/send')
def mail_test_send():
    msg = Message(subject='Salut bydoo !',
                  body='Ca marchass !',
                  sender="notify@around.me",
                  recipients=["bydooweedoo@gmail.com"])
    result = mail.send(msg)
    return 'Result: %s' % result